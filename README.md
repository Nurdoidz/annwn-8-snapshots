# Annwn VIII Snapshot-lapse

## How to Capture Snapshots

1. Load Minecraft 1.15.2+ with a 16:9 resolution (3840×2160 recommended), with [Chroma Hills](http://chromahills.com/) resource pack enabled, and load the “Spawn08” world.
2. With or without cheats:  
**With cheats:** Teleport to `XYZ: -130.3 / 122 / 108.299`.
**Without cheats:** Navigate to `XYZ: -130 / 122 / 108`. There is a patch of snow. While sneaking, move closest to the north-west edge of the block. Your coordinates should be proceeded by (or close to) `.300`. Release sneaking.
3. Adjust your camera with a yaw of 140.4° and a pitch of 18.2°. You should be looking at the center of the city.
4. Adjust the FOV to 31.
5. Take a screenshot.